#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
    int sock;
    struct sockaddr_in addr;
    char buf[1024];
    int bytes_read;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock < 0)
    {
        puts("Cant create socket");
        return 0;
    }

    // Set socket to broadcast mode
    // Makes possible to send one message for all clients
    int true_flag = 1;

    if (-1 == setsockopt(sock,
            SOL_SOCKET,
            SO_BROADCAST,
            &true_flag,
            sizeof(true_flag))) {
        puts("Error in setting Broadcast option");
        close(sock);
        return 0;
    }

    // Set socket to reuseport mode
    // Makes possible to run several clients at the same time
    if (-1 == setsockopt(sock,
            SOL_SOCKET,
            SO_REUSEPORT,
            &true_flag,
            sizeof(true_flag))) {
        puts("Error in setting Reuseport option");
        close(sock);
        return 0;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(8000);
    addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    if(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        puts("Cant bind to the host");
        close(sock);
        return 0;
    }

    while(1)
    {
        bytes_read = recvfrom(sock, buf, 1024, 0, NULL, NULL);
        buf[bytes_read] = '\0';
        puts(buf);
    }

    close(sock);
    return 0;
}
